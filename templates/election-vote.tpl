<p>Please use the form below to cast your votes for the {$election_year}
SUCS executive committee. Note that you have to be a <strong>current student
member</strong> of SUCS in order for your vote to be valid.<br />

(Click on a candidate's name to view their manifesto)</p>

<form method="post" action="{$smarty.server.REQUEST_URI}">
{foreach name=outer key=position item=cand from=$candidate}
<h2>{$position|capitalize}</h2>
	{if count($cand) > 1}
		{foreach item=person from=$cand}
<input type="radio" name="{$position}" value="{$person.username}" id="{$position}{$person.username}" /><label for="{$position}{$person.username}"><a href="{$person.manifesto_link}">{$person.realname}</a> ({$person.username})</label><br />
		{/foreach}
<input type="radio" name="{$position}" value="abstain" id="{$position}abstain" /><label for="{$position}abstain"><em>Abstain</em></label><br />
	{else}
There is no vote for {$position|capitalize} as only one candidate was nominated:<br />
<a href="{$cand.0.manifesto_link}">{$cand.0.realname}</a> ({$cand.0.username})
	{/if}

{/foreach}

<div class="row">
	<label for="vote_passwd">SUCS Password:</label>
	<span class="textinput">
		<input name="vote_passwd" id="vote_passwd" type="password" />
		<div class="note">Your SUCS password is required to ensure that the vote is from you.</div>
	</span>
</div>
<div class="row">
	<span class="textinput">
		<input type="submit" value="Cast Votes" name="submit" />
	</span>
</div>

</form>
&nbsp;