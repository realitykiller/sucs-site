<form class="admin" action="{$path}" method="POST">
	<div class="row">
		<label for="edittitle">Title</label>
		<span class="textinput"><input type="text" name="edittitle" id="edittitle" value="{$title}" style="width: 100%;" disabled="disabled" /></span>
	</div>
	<div class="row">
		<label for="summary">Summary</label>
		<span class="textinput"><input type="text" name="summary" id="summary" value="{$record.summary|escape:'htmlall'}" style="width: 100%;" /></span>
	</div>
	<div class="row">
		<label for="body">Content</label>
		<span class="textinput"><textarea name="body" id="body" style="width: 100%; height: 25em;">{$editcontent|escape:'htmlall'}</textarea></span>
	</div>
	<div class="row">
		<span class="textinput">
			<input type="submit" id="submit-1" name="action" value="Save" />
		</span>
	</div>
	<div class="clear"></div>
</form>
