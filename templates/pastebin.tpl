{if isset($userInfo)}
	<div id="userInfo">
		<h3> {$userInfo}</h3>
		<a href="{$uri}{$id}">{$uri}{$id}</a>
		<a href="{$uri}{$id}/plain">(plain)</a>
	</div>

	<div id="displayCode">
		<ol>
{foreach name=highlightedCode from=$highlightedCode item=codeLine}
			<li> &nbsp; {$codeLine} </li>
{/foreach}
		</ol>
	</div>
{/if}

{if $session->loggedin}
	<form action="{$baseurl}{$path}" method="post">
		<div class="box">
			<div class="boxhead"><h2>Paste an item</h2></div>
			<div class="boxcontent">
			<p>Please keep your code snippets minimal, especially in the cases of
			coursework solution problems.</p>
			<div class="row">
			<label for="lstLanguage">Language:</label>
			<span class="textinput">
			<select name="language" id="lstLanguage">
				<option value="NONE">NONE</option>
{if isset($selectedLanguage) }
	<option value="{$selectedLanguage.key}" selected="selected">{$selectedLanguage.lang} </option>
{/if}
{foreach name=pasteLanguages from=$pasteLanguages item=pasteLanguage key=pasteLangKey}
				<option value="{$pasteLangKey}"> {$pasteLanguage} </option>
{/foreach}
			</select></span>
			</div>
			<div class="row">
			<label for="txtCode">Code:</label>
			<span class="textinput">
			<textarea name="code" id="txtCode" rows="15" style="width: 100%;">{$code}</textarea>
			</span>
			</div>
			<div class="row">
			<label for="retainDay">Keep For :</label>
			<span class="textinput">
			<input type="radio" value="day" id="retainDay" name="retain[]" />
			Day
			<input type="radio" value="week" checked="checked" id="retainWeek" name="retain[]" />
			Week
			<input type="radio" value="month" id="retainMonth" name="retain[]" />
			Month
			<input type="radio" value="forever" id="retainForever" name="retain[]" />
			Forever
			</span>
			</div>
			<div class="row">
			<span class="textinput"><input type="submit" value="Submit"/></span>
			</div>
			<div class="clear"></div>
			</div>
			<div class="hollowfoot"><div><div></div></div></div>
		</div>
	</form>
{else}
<p>You must be logged in to paste new items to the PasteBin</p>
{/if}
