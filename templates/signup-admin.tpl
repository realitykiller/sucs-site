{if $staff == TRUE}
<form action="" method="post">
<table border='1'>
<tr><th>ID</th><th>SID</th><th>Type</th><th>Issued By</th><th>Card Number</th></tr>
{foreach name=signups from=$signups item=signup}
<tr>
    <td>{$signup.id|escape}</td>
    <td><input type="text" size="10" name="sid:{$signup.id|escape}" value="{$signup.sid|escape}" /></td>
    <td>{$signup.typename|escape}</td>
    <td>{$signup.username|escape}</td>
    <td>{$signup.card|escape}</td>
</tr>
{/foreach}
</table>
<input type='hidden' name='command' value='update' />
<input type='submit' value='Update Records'/>
<input type='reset' value='Reset'/>
</form>
{else}
        <div class="errorbar">
                <div><div><div>
                        You must be logged in and be a staff member to modify signup details;
                </div></div></div>
        </div>
{/if}

