</div>

<div id="navigation">
{include file="menu.tpl"}

{if $session->loggedin}
	<div style="resize: both;">
		{include file="feedback.tpl"}
	</div>
{/if}

{* include file="valid.tpl" *}

	</div>

<div class="clear"></div>
</div>

<div id="footer">
	<div class="cornerBottomLeft">
	<div class="cornerBottomRight">
		<p>Site designed and maintained by SUCS. All opinions expressed are those of the relevant individual and not of the society.</p>
		{if isset($totaltime)}<p>Script executed in {$totaltime} seconds</p>{/if}
	</div>
	</div>
</div>

{if $action=="edit"}
        <!--
        <script language="javascript" type="text/javascript" src="{$baseurl}/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        <script language="javascript" type="text/javascript" src="{$baseurl}/js/tiny_mce.js"></script>
        --!>
	<script language="javascript" type="text/javascript" src="{$baseurl}/js/jquery-2.1.3.min.js"></script>
        <script language="javascript" type="text/javascript" src="{$baseurl}/js/ckeditor/ckeditor.js"></script>
	<script language="javascript" type="text/javascript" src="{$baseurl}/js/ckeditor.js"></script>
{/if}



{if $debug}{debug}{/if}
</body>
</html>
