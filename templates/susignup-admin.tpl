{if $staff == TRUE}
 {if $mode == 'error'}
	<div class='errorbar'>
		<strong>Error: </strong> {$error_text}
	</div>
 {/if}
 {if $mode == 'renewals'}
<form action='{$componentpath}' method='post'>
 <table id='susignup-renewals'>
	<thead>
		<tr><th>Renew Now?</th><th>Name (SU)</th><th>Name (SUCS)</th><th>Student ID</th><th>SUCS Username</th><th>Paid Until</th></tr>
	</thead>
	<tbody>
{foreach from=$matches item=match}
		<tr><td class='centre'><input name='renew[]' id='renew' value='{$match[3]}' type='checkbox' checked='checked'/></td><td>{$match[0]}</td><td>{$match[1]}</td><td class='centre'>{$match[2]}</td><td>{$match[3]}</td><td class='centre'>{$match[4]}</td></tr>
{foreachelse}
		<tr><td colspan='6'><em>No matches found</em></td></tr>
{/foreach}
	</tbody>
	<tfoot>
		<tr><td colspan='5'><strong>For large numbers of renewals, this can take a while!</strong></td><td class='centre'><input type='submit' name='rwsubmit' id='rwsubmit' value='Go!' /></td></tr>
	</tfoot>
</table>
<input type='hidden' name='mode' id='mode' value='renewals2' />
</form>
{if $others > 0}
<p>There were also <strong>{$others}</strong> other records found.<br />
Of these, {$paidup} are current members and {$pending} are pending signups.</p>
{/if}
 {elseif $mode == 'renewals2'}
<div class='centre'>
<div class='cbb' style='text-align: left'>
<h3> Mass renewal results </h3>
<strong>{$attempt}</strong> accounts marked for renewal<br />
<hr />
{if $successes == $attempt}
<strong>All</strong> of these requests were successful.<br />
{else}
<strong>{$successes}</strong> requests were successful, and <span style='color: red'><strong>{$failures}</strong></span> requests failed.<br />
The following accounts failed to renew:
<ul>
{foreach from=$failusers item=f}
<li>{$f}</li>
{/foreach}
</ul>
{/if}
</div>
</div>
 {elseif $mode == 'list'}

 <table id='susignup-renewals'>
	<thead>
		<tr><th>Name (SU)</th><th>Name (SUCS)</th><th>Student ID</th><th>SUCS Username</th><th>Paid Until</th></tr>
	</thead>
	<tbody>
{foreach from=$matches item=match}
		<tr><td>{$match[0]}</td><td>{$match[1]}</td><td class='centre'>{$match[2]}</td><td>{$match[3]}</td><td class='centre'>{$match[4]}</td></tr>
{foreachelse}
		<tr><td colspan='6'><em>Oh Noes! No members found in the SU database</em></td></tr>
{/foreach}
	</tbody>
</table>
 {elseif $mode == 'search'}
<div style='text-align: center'>
 <div class='cbb' style='text-align: left'>
	Student {$sid} appears to have paid, and a signup slip has been generated.<br />
	The details are:<br />
	<strong>Slip ID: </strong> {$slipid}<br />
	<strong>Slip Password: </strong> {$slippass} <br />
	<form action="https://sucs.org/signup/" method="post">
		<input type=hidden name="signupid" id="id" value="{$slipid}" />
		<input type=hidden name="signuppw" id="pass" value="{$slippass}" />
		<input type=submit name="submit" value="Proceed" />
	</form>
 </div>
</div>
 {elseif $mode == 'menu' || $mode == 'error' || $mode == 'renew'} 
	<p>
This component allows staff members to sort out memberships and renewals that have been paid for via the SU payments system. </p>
<p>Renew an individual member, generate a list of members that look like they've paid via the SU system or begin the signup process for a newbie who doesn't have their transaction ID by selecting the appropriate option below.
	</p>
<div style='text-align: center'>
<div class='susignup-admin-menu cbb'>
<h3>Signup a newbie</h3>
<form action='{$componentpath}' method='post'>
	<label for='sid'>Student Number:</label>
	<input type='text' name='sid' id='sid' maxlength=6 /> 
	<input type='hidden' name='mode' id='mode' value="search" />
	<input type='submit' name='snsubmit' id='snsubmit' value="Start" />
</form>
</div>
<div class='susignup-admin-menu cbb'>
<h3>Renew existing account:</h3>
<form action='{$componentpath}' method='post'>
	<label for='member'>Account:</label>
	{html_options name=member options=$renewables}
	<input type='hidden' name='mode' id='mode' value="renew" />
	<input type='submit' name='rnsubmit' id='rnsubmit' value="Renew" />
</form>
</div>
<div class='susignup-admin-menu cbb'> 
<h3>Bulk Renewals</h3>
<p style='text-align: left'>This will query the SU API and generate a list of corresponding SUCS accounts to be renewed.<br /> This process can take a little while to run</p><br />
<form action='{$componentpath}' method='post'>
	<input type='submit' name='rwsubmit' id='rwsubmit' value='Generate List' />
	<input type='hidden' name='mode' id='mode' value='renewals' />
</form>
</div>
<div class='susignup-admin-menu cbb'> 
<h3>SU Membership List</h3>
<p style='text-align: left'>This will query the SU API for all current SUCS members<br /> This process can take a little while to run</p><br />
<form action='{$componentpath}' method='post'>
	<input type='submit' name='lssubmit' id='lssubmit' value='View List' />
	<input type='hidden' name='mode' id='mode' value='list' />
</form>
</div>
</div>
 {else}
	<div class="errorbar">
		Invalid mode - {$mode}
	</div>
 {/if}
{else}
        <div class="errorbar">
                <div><div><div>
                        You must be logged in and be a staff member to use this component;
                </div></div></div>
        </div>
{/if}

