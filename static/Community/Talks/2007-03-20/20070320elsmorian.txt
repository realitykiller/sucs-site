<div class="box">
        <div class="boxhead"><h2>Chris Elsmore (elsmorian) - &quot;Music, Media &amp; DRM&quot;</h2></div>
        <div class="boxcontent">
        <p>Chris Elsmore presents a look at the history of music recording, and how it&#39;s progressed through the ages.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-03-20/elsmorian.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-03-20/welshbyte.flv" image="/videos/talks/2007-03-20/welshbyte_preview.png" id="player" displayheight="240">
<param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-03-20/elsmorian.flv" />
<param name="image" value="/videos/talks/2007-03-20/elsmorian_preview.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-03-20/elsmorian_preview.png" />
</object></div>

<p><strong>Length: </strong>10m 51s</p><p><strong>Video: </strong><a href="/videos/talks/2007-03-20/2007-03-20-elsmorian.ogg" title="392x288 Ogg Theora - 43.4MB">392x288</a> (Ogg Theora, 43.4MB)<br /><strong>Slides:</strong><a href="/videos/talks/2007-03-20/2007-03-20-elsmorian-slides.pdf" title="Music, Media and DRM slides">2007-03-20-elsmorian-slides.pdf</a> (PDF, 2MB)<br /></p></div>
        <div class="boxfoot"><p>&nbsp;</p></div>
</div>
