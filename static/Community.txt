<h3>These are some of the ways you can get involved in the SUCS
Community</h3>
<dl>
<dt><a href="Community/Milliways">Milliways (chat room)</a></dt>
<dd>Join us on the SUCS talker system, Milliways. It's an old school chat system that was first conceived in 1992 and is still seeing around-the-clock action. This is where the older life members usually hang out so it's a good place to find some experienced advice, coding tips etc. </dd>
<dt><a href="Community/Talks">SUCS Lightning Talks</a></dt>
<dd>A great way to share things you are passionate about with fellow members. We've had 3 Lightning Talks so far in the past 2 years and each of them have been very well received.  Footage of the talks can also be found in this section.</dd>
<dt><a href="Community/Projects">SUCS Projects</a></dt>
<dd>SUCS hosts a number of open source coding projects and offers its members Git & Subversion repositories. To enhance these repositories we also provide Gitlab Projects & the Trac wiki/ticket tracking system to assist in the management of projects. The projects page lists a number of projects which SUCS members are currently working on, and which you can get involved with.</dd>
<dt>Socials</dt>
<dd>We regularly meet in JCs every Wednesday at 1PM and most Fridays at 7PM. It's a great chance to get to know everyone and have a chat with people. </dd>
</dl>
<h3>Other ways</h3>
<ul>
<li>We have a page on <a href="https://www.facebook.com/SwanseaUniversityComputerSociety" title="SUCS">Facebook</a></li>
<li>Through our <a href="https://twitter.com/SUCSExec">Twitter</a></li>
</ul>