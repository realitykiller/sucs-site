<p>Welcome to the Tools page. Here are some short descriptions of the tools available on sucs.org. Click the link in the menu bar for more information on the tool you wish to use.</p>
<h2><a href="http://lists.sucs.org/">Mailing Lists</a></h2>
<p>A directory of all the mailing lists hosted by SUCS. Using this you can find out more about a particular mailing list, subscribe or unsubscribe to a list and change your preferences for any lists you subscribed to. If you are an admin of a mailing list you can access the admin interface through this page.</p>
<h2><a href="Tools/ShortURI">ShortURI</a></h2>
<p>Make long web addresses shorter. Useful if you wish to paste it to Milliways, or perhaps a forum or chatroom elsewhere; you can use ShortURI to create a shorter version that will direct you to the original address.</p>
<h2><a href="https://sucs.org/webmail">Web Mail</a></h2>
<p>Check your SUCS e-mail account via the web using <cite>Roundcube</cite>. Learn more about <a href="Knowledge/Help/SUCS%20Services/Accessing%20your%20email">accessing your SUCS e-mail</a> in our Help section.</p>
<h2><a href="Tools/PasteBin">PasteBin</a></h2>
<p>If you are having trouble with some code and have gone to Milliways for help, use the Pastebin to avoid spamming Milliways with lines of code, plus it also has a nifty syntax highlighting feature. </p>
<h2><a href="Tools/Desktop%20on%20Demand">Desktop on Demand</a></h2>
<p>Desktop on Demand allows you to connect to one of the SUCS computers remotely to run linux applications from anywhere. For more information see <a href="Knowledge/Help/SUCS%20Services/Using%20Desktop%20on%20Demand">Using Desktop on Demand</a>.</p>