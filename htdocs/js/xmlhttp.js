function postcomment(strBaseUrl, strBlog, strPost) {
	var xmlhttp = false;

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
		xmlhttp.overrideMimeType('text/xml');
	} else if (window.ActiveXObject) {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		return true;
	}

	document.getElementById("spinner").style.visibility = "visible";
	document.getElementById("errors").innerHTML = "";
	frm = document.forms['commentform'];
	url = "blog=" + strBlog + "&post=" + strPost + "&author=" + escape(frm.elements['author'].value) + "&email=" + escape(frm.elements['email'].value) + "&comment=" + escape(frm.elements['comment'].value);

	xmlhttp.open("POST", strBaseUrl+"comment", true);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			//alert(xmlhttp.responseText);
			//alert(xmlhttp.getAllResponseHeaders());
			document.getElementById("spinner").style.visibility = "hidden";
			frm.elements['author'].style.backgroundColor = "#FFF";
			frm.elements['email'].style.backgroundColor = "#FFF";
			frm.elements['comment'].style.backgroundColor = "#FFF";
			responseArray = xmlhttp.responseText.split("<split>");
			if(responseArray[1] == "OK"){
				document.getElementById("comments").innerHTML += responseArray[0];
				document.getElementById("errors").innerHTML = "";
				//frm.elements['author'].value = ""
				//frm.elements['email'].value = "";
				frm.elements['comment'].value = "";
			} else {
				if(responseArray[2]!="") {
					frm.elements[responseArray[2]].style.backgroundColor = "#FFD1CD";
					document.getElementById("errors").innerHTML = responseArray[0];
				} else {
					document.getElementById("comments").innerHTML += responseArray[0];
				}
			}
		}
	}
	xmlhttp.send(url)
	return false;
}
