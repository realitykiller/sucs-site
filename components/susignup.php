<?php

//unique society identifier
$GroupingID = "6613";

include "../lib/member_functions.php";
include "../lib/date.php";
include "../suapi.inc.php";

$error_email = "admin@sucs.org";
$mode = 'login';
//login
if(!empty($_REQUEST['sid'])&&!empty($_REQUEST['transactionID'])){
//set signup details

	$sid = $_REQUEST['sid'];
        $transactionID = $_REQUEST['transactionID'];

	//set POST variables
	$url = "https://$suapi_user:$suapi_pass@su-api.sucs.org/memberships/Membership.asmx/GetMemberByTransactionID";
	$fields = array(
	       'transactionID'=>urlencode($transactionID),
	       'GroupingID'=>urlencode($GroupingID),
	       );
	//url-ify the data for the POST
	$field_string = "";
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string,'&');
	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST,count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	//execute post
	$result = curl_exec($ch);

	//close connection
	curl_close($ch);
	if(!$result){
		$mode = "error";
		$smarty->assign("error_text", "Invalid query returned");
	} else {
		libxml_use_internal_errors(true);
		$xml=simplexml_load_string($result);
		if (!$xml||isset($xml->head->title)) {
			$mode='error';
			$smarty->assign("error_text", "An error occurred communicating with the SUSU API");		
		} else{
			$re1='.*?';
			$re2='(\\{.*?\\})';
			if ($c=preg_match_all ("/".$re1.$re2."/is", $result, $matches))
			{
			  	$json_string=$matches[1][0];
			} else{
			  $mode = "error";
			}
			$array = json_decode($json_string,TRUE);
			if($array['cardNumber'] == $sid && $sid != ""){
				$mode = 'form';

				// SET THIS FOR DEBUG MODE
				// $sucsDB->debug = true;

				$fullName = $array['firstName']." ".$array['lastName'];
				$query = "INSERT INTO transactions (transactionID,fullName,emailAddress,cardNumber,personID) values(?, ?, ?, ?, ?)";
				$attribs[] = $transactionID;
				$attribs[] = $fullName;
				$attribs[] = $array['emailAddress'];
				$attribs[] = $array['cardNumber'];
				$attribs[] = $array['personID'];

				$valid = true;
				if(!$sucsDB->Execute($query,$attribs)) {
					// We did this because Adodb ErrorNo command is broken
					$query = "SELECT * FROM transactions WHERE transactionID = ?";
					unset($attribs);
					$attribs[]=$transactionID;
					if($sucsDB->Execute($query,$attribs)) {

						$query = "SELECT * FROM transactions WHERE transactionID = ?";
						$attribs= array();
						$attribs[]=$transactionID;
						$data = $sucsDB->GetAll($query,$attribs);
						if(is_array($data)&&sizeof($data)==1&&$data[0]['signupid']==NULL){
							// As it stands this shouldn't happen but if it does, no reason not to let them carry on
							$valid = true;
						} elseif(is_array($data)&&sizeof($data)==1&&$data[0]['signupid']!=NULL) {
							$query = "SELECT * FROM signup WHERE sid = ?";
							unset($attribs);
							$attribs[]=$sid;
							$data = $sucsDB->Execute($query, $attribs);
							if(!$data) {
								mail(
									$error_email,
									"SUSignup Error",
									"An error occured in the susignup system at stage 0\n Love the signup system.\n\nP.S. The Error is:\n ".$sucsDB->ErrorMsg(),
									"From: \"SUCS Admin\" <admin@sucs.org>"
							);
								$valid=false;
							} else if($data->fields['username']!=NULL){
								$smarty->assign("username", $data->fields['username']);
								$mode='numpty2';		
								// This happens if they already ahve a SUCS account and are trying to create a new signupid
								$valid = false;
							} else {
								$smarty->assign("id", $data->fields['id']);
								$smarty->assign("pass", $data->fields['password']);
								$mode='numpty';
								// If this happens they tried to get a new signupid/pass after already getting one, and should get weekly reminders about it  
								$valid = false;
							}
						} else {
							$mode="error";
							mail(
								$error_email,
								"SUSignup Error",
								"An error occured in the susignup system at stage 1\n Love the signup system.\n\nP.S. The Error is:\n ".$sucsDB->ErrorMsg(),
								"From: \"SUCS Admin\" <admin@sucs.org>"
							);

							$valid = false;
						}

					} else {
						$mode="error";
						mail(
							$error_email,
							"SUSignup Error",
							"An error occured in the susignup system at stage 2\n Love the signup system.\n\nP.S. The Error is:\n ".$sucsDB->ErrorMsg(),
							"From: \"SUCS Admin\" <admin@sucs.org>"
						);

						$valid = false;
					}
				}

				if($valid)
				{
					unset($attribs);
					unset($query);
					$query = "SELECT * FROM members WHERE sid = ?";
					$attribs[] = $sid;
					unset($data);
					$data = $sucsDB->Execute($query, $attribs);
					if (!$data) {
						$mode = "error";
						$smarty->assign("error_text", "Error in internal database check");
					} elseif ($data->RecordCount()==1) {
						$mode = "renew";
						$q="UPDATE members SET paid=?, lastupdate=DEFAULT, lastedit=? WHERE username=?;";
        					$r=$sucsDB->Execute($q, array(paidUntil(time()), '99999', $data->fields['username']));
						if (!$r) {
							mail(
								$error_email,
								"SUSignup Error",
								"An error occured in the susignup system at stage 2.5\n Love the signup system.\n\nP.S. The Error is:\n ".$sucsDB->ErrorMsg(),
								"From: \"SUCS Admin\" <admin@sucs.org>"
							);

					        } else {
							$q="SELECT email, typename FROM members, member_type WHERE username=?  AND type=member_type.id;";
							$r=$sucsDB->Execute($q, array($data->fields['username']));

							$message  = "Account Renewal notification\n\n";
							$message .= "Account   : ".$data->fields['username']."\n";
							$message .= "User Type : ".$r->fields['typename']."\n";
							$message .= "Renewed by: ".$data->fields['username'].".\n\n";
							$message .= "**** Payment was made via the SU payments system ****\n";
							$message .= "Regards\n  The SU Renewals script";
							mail("treasurer@sucs.org","Account Renewal",$message);

							$message = "Your Swansea University Computer Society (SUCS) membership has been renewed\n\n";
							$message .= "Username: ".$renew_name."\n";
							$message .= "If you do not know or have forgotten your password, please email admin@sucs.org to arrange for it to be changed.\n\n";
							$message .= "Regards\n  The SUCS admin";
							$header = "From: admin@sucs.org\r\n";
							$header .= "Reply-To: admin@sucs.org";
							// Personal account
							mail($r->fields['email'],"SUCS account renewal",$message,$header);
							// sucs account
							mail($data->fields['username']."@sucs.org","SUCS account renewal",$message,$header);
							unset($query);
							unset($attribs);
							$query = "UPDATE transactions set signupid=? WHERE transactionID=?";
							$attribs[]="000";
							$attribs[]=$transactionID;
							$data = $sucsDB->Execute($query,$attribs);
							if (!$data) { 
								$mode="error";
								mail(
									$error_email,
									"SUSignup Error",
									"An error occured in the susignup system at stage 2.6\n Love the signup system.\n\nP.S. The Error is:\n ".$sucsDB->ErrorMsg(),
									"From: \"SUCS Admin\" <admin@sucs.org>"
								);
							}
				        	}
					} else {
						$pass = make_password();
						unset($query);
						$query = "insert into signup (password,sid,issuedby) values( ?, ?, ?) returning id";
						unset($attribs);
						$attribs[]=$pass;
						$attribs[]=$array['cardNumber'];
						$attribs[]='99999';

						$iddata = $sucsDB->Execute($query,$attribs);
						$id = $iddata->fields['id'];
						if (!$iddata) {
							$mode="error";
							mail(
								$error_email,
								"SUSignup Error",
								"An error occured in the susignup system at stage 3\n Love the signup system.\n\nP.S. The Error is:\n ".$sucsDB->ErrorMsg(),
								"From: \"SUCS Admin\" <admin@sucs.org>"
							);
						} else {
							$query = "update transactions set signupid=? WHERE transactionID=?";
							unset($attribs);
							$attribs[]=$id;
							$attribs[]=$transactionID;
							if(!$sucsDB->Execute($query,$attribs)) {
								$mode="error";
								mail(
									$error_email,
									"SUSignup Error",
									"An error occured in the susignup system at stage 4\n Love the signup system.\n\nP.S. The Error is:\n ".$sucsDB->ErrorMsg(),
									"From: \"SUCS Admin\" <admin@sucs.org>"
								);
								print($sucsDB->ErrorMsg());
							} else {
								$smarty->assign("id", $id);
								$smarty->assign("pass", $pass);
								mail(
									$array['emailAddress'].','.$sid.'@swansea.ac.uk',
									"SUCS Signup Information",
									"Thankyou for joining Swansea University Computer Society, your signup details are below;\nSignupID: $id\nSignup Password: $pass\nIf you have successfully completed signup immediately then you can disregard this message.\n\nSUCS Admin Team.",
									"From: \"SUCS Admin\" <admin@sucs.org>"
								);
							}
						}
					}
				}
			}
		}
	}
}

$smarty->assign("mode", $mode);
$output = $smarty->fetch("susignup.tpl");

$smarty->assign("title", "Join");
$smarty->assign("body", $output);


?>
