<?php

include_once("../lib/members.php");

$members = new Members;

/* Banana viewing component. Mostly obsolete since most of this is now in the members component */

$banana_index = array_search("Bananas", $pathlist);
if ((sizeof($pathlist) > $banana_index + 1) && (trim($pathlist[$banana_index + 1]) != "")) {
	$mode = "detail";
	$who = $pathlist[$banana_index + 1];
} else {
	// User wasn't looking for anything in particular, so bump them to the Members page
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://$preferred_hostname$baseurl/Community/Members/");
}

/* redirect requests for members' bananas to Members component. Only serve banana info for non-members (e.g. LIS ;-)) */

if (count($members->memberView($who)) > 0) {
	// this is actually a real member, so serve a redirect and let Members handle it
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://$preferred_hostname$baseurl/Community/Members/$who");
} else {
	// banana admin bits 
	if ($mode == "detail") {
		if ($session->loggedin) {
			include("../lib/banana-admin.php");
		}
	}
	// attempt to find some bananas for this name in the database
	$smarty->assign("awards",$DB->GetAll("SELECT *, date_trunc('second', whn) FROM awards WHERE username=? ORDER BY whn DESC", array($who)));
}


if (isset($who))
	$smarty->assign('who', "$who");


$smarty->assign('mode', "$mode");
$smarty->assign('users', $users);

$result = $smarty->fetch('bananas.tpl');
$smarty->assign('title', "Banana Awards");
$smarty->assign('body', $result);



?>
