<?php
$body = "";
$email_to = "joinus@sucs.org";
$_REQUEST['email'] = str_replace("\n", "", $_REQUEST['email']);
$headers = "From: ".$_REQUEST['email']."\n"."Reply-to: ".$_REQUEST['email'];
$subject = "Join request for user ".$_REQUEST['uname'];

// Perform data integrity checks

// Full name
if (preg_match("/[^\p{L}- ]/iu", $_REQUEST['realname'])) {
	$body .= "<p>That's a suspiciously unusual-looking name, <strong>" . htmlentities($_REQUEST['realname']) . "</strong>.</p>\n";
	$body .= "<p>Please click <em>Back</em> and correct it.</p>\n";
} else {
// Student number
if (!is_numeric($_REQUEST['student_number']) || strlen($_REQUEST['student_number']) <>  6) {
	$body .= "<p>The student number <strong>" . htmlentities($_REQUEST['student_number']) . "</strong> appears not to be valid.</p>\n";
	$body .= "<p>Please click <em>Back</em> and correct it.</p>\n";
} else {
// Email address
	list($username, $maildomain) = preg_split("/@/", $_REQUEST['email']);
	if (!checkdnsrr($maildomain, "MX") && !checkdnsrr($maildomain)) {
		$body .= "<p>The email address <strong>" . htmlentities($_REQUEST['email']) . "</strong> appears not to be valid.</p>\n";
		$body .= "<p>Please click <em>Back</em> and correct it.</p>\n";
	} else {
// Username
		// This test is rather inadequate. Needs improving
		if (!preg_match("/^[a-z0-9_]*$/i", $_REQUEST['uname'])) {
			$body .= "<p>Usernames can only contain letters, numbers and underscores(_).</p>\n";
			$body .= "<p>Please click <em>Back</em> and choose a different one.</p>\n";
		} else {
// We're good to go
			$address = "http";
			if ($_SERVER['HTTPS']) $address .= "s";
			$address .= "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ;
			$email_body = "This email was automatically created by the SUCS Website.\n\n";
			$email_body .= "It was created by " . $address . "\n";
			$email_body .= "from IP " . $_SERVER['REMOTE_ADDR'] . " on " . date("r") . "\n\n";
			$email_body .= "The details of the request are as follows:\n\n";
			$email_body .= "  Real name ............ {$_REQUEST['realname']}\n";
			$email_body .= "  Student number ....... {$_REQUEST['student_number']}\n";
			$email_body .= "  Email address ........ {$_REQUEST['email']}\n";
			$email_body .= "  Requested username ... {$_REQUEST['uname']}\n\n";
			$email_body .= "Please reply to this email, providing the user with details of\n";
			$email_body .= "how to join.\n\n";
			$email_body .= "-- \nSUCS Website\n";

			mail($email_to, $subject, $email_body, $headers);

			$body .= "<p>Thank you for your request to join SUCS.</p>";
			$body .= "<p>Someone from our admin team will email you shortly.</p>";
		} // username
	} // email
} // student number
} // full name

$smarty->assign("body", $body);
$smarty->assign("title", "Joining");
$pathlist[2] = "Joining";
$smarty->assign("subselect", "Joining");
?>
